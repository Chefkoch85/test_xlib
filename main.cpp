
#include <X11/X.h>
#include <X11/Xlib.h>
#include <cstdio>
#include <cstring>
#include <err.h>
#include <map>
#include <utility>

#include "xwin.h"
#include "log.h"

#define LOGIEV(msg) CLog::i("Event.type: %02d, win: 0x%08lX..%s", ev.type, ev.xany.window, msg)

struct CommonVars
{
    Display* display;
    Window win;

    void set(Display* dis, Window w)
    {
        display = dis;
        win = w;
    }
};

// typedef void (*button_cb)(void* sender);
using button_cb = void (*)(void* sender);
struct SubWindowButton
{
    CommonVars common;
    unsigned int back_color, back_color_hover;
    button_cb cb = nullptr;

    void set(Display* dis, Window w)
    {
        common.set(dis, w);
    }

    Window get_win()
    {
        return common.win;
    }
};

bool run = true;
void button_release(void* sender)
{
    run = false;
}

using win_map = std::map<Window, SubWindowButton>;


int main(int argc, char** argv)
{
    CLog::open("log.txt");
    printf("Hello linux\n");

    if (open_display())
        CLog::c(true, "Error [%02X]: %s\n", 1, "opening display connection failed");

    Window main_win;
    win_map sec_win;
    if ((main_win = create_window(0,0,800,600,5,0,"A House without windows..")) == 0)
        CLog::c(true, "Error [%02X]: %s\n", 2, "creating main_win failed");

    set_size_hint(main_win, 800, 600, 1600, 600);
    printf("Window: 0x%08lX\n", main_win);

    SubWindowButton swb;
    swb.set(get_dis(), create_window(10, 10, 100, 100, 5, main_win, nullptr));
    sec_win.insert(std::make_pair(swb.common.win, swb));
    swb.set(get_dis(), create_window(120, 35, 100, 50, 5, main_win, nullptr));
    swb.cb = button_release;
    sec_win.insert(std::make_pair(swb.common.win, swb));
    swb.set(get_dis(), create_window(230, 10, 100, 100, 5, main_win, nullptr));
    sec_win.insert(std::make_pair(swb.common.win, swb));
    
    u32 bc[] = { 0xFF008000, 0xFF800000, 0xFF000080 };
    u32 bch[] = { 0xFF00FF00, 0xFFFF0000, 0xFF0000FF };
    u32 i = 0;
    for (auto& w : sec_win)
    {
        w.second.back_color = bc[i];
        w.second.back_color_hover = bch[i];
        XSetWindowBackground(get_dis(), w.second.get_win(), w.second.back_color);
        i++;
    }

    GC gc = DefaultGC(get_dis(), DefaultScreen(get_dis()));

    auto txt = "Maybe?";
    XDrawString(get_dis(), main_win, gc, 0, 300, txt, strlen(txt));

    XEvent ev;
    i32 w = 0, h = 0;
    while (run)
    {
        while (XPending(get_dis()))
        {
            XNextEvent(get_dis(), &ev);
            
            switch (ev.type)
            {
            case KeyPress:
                LOGIEV("KeyPress");
                break;
            case KeyRelease:
                LOGIEV("KeyRelease");
                break;

            case ButtonPress:
                LOGIEV("ButtonPress");
                break;
            case ButtonRelease:
                LOGIEV("ButtonRelease");
                if (sec_win[ev.xany.window].cb)
                    sec_win[ev.xany.window].cb(&sec_win[ev.xany.window]);
                break;

            case MotionNotify:
                LOGIEV("MotionNotify");
                break;
            case EnterNotify:
                LOGIEV("EnterNotify");
                if (ev.xany.window != main_win)
                {
                    XSetWindowBackground(get_dis(), ev.xany.window, sec_win[ev.xany.window].back_color_hover);
                    XClearWindow(get_dis(), ev.xany.window);
                }
                break;
            case LeaveNotify:
                LOGIEV("LeaveNotify");                
                if (ev.xany.window != main_win)
                {
                    XSetWindowBackground(get_dis(), ev.xany.window, sec_win[ev.xany.window].back_color);
                    XClearWindow(get_dis(), ev.xany.window);
                }
                break;

            case FocusIn:
                LOGIEV("FocusIn");
                break;
            case FocusOut:
                LOGIEV("FocusOut");
                break;

            case KeymapNotify:
                LOGIEV("KeymapNotify");
                break;

            case Expose:
                LOGIEV("Expose");
                XClearWindow(get_dis(), main_win);
                XSetForeground(get_dis(), gc, 0xFFFFFFFF);
                XFillRectangle(get_dis(), main_win, gc, w / 4, h / 4, w / 2, h / 2);
                XFlush(get_dis());
                break;
            case GraphicsExpose:
                LOGIEV("GraphicsExpose");
                break;
            case NoExpose:
                LOGIEV("NoExpose");
                break;

            case VisibilityNotify:
                LOGIEV("VisibilityNotify");
                break;

            case CreateNotify:
                LOGIEV("CreateNotify");
                break;
            case DestroyNotify:
                LOGIEV("DestroyNotify");
                break;

            case UnmapNotify:
                LOGIEV("UnmapNotify");
                break;
            case MapNotify:
                LOGIEV("MapNotify");
                break;
            case MapRequest:
                LOGIEV("MapRequest");
                break;
            case ReparentNotify:
                LOGIEV("ReparentNotify");
                break;

            case ConfigureNotify:
            {
                LOGIEV("ConfigureNotify");
                w = ((XConfigureEvent*)&ev)->width;
                h = ((XConfigureEvent*)&ev)->height;
                CLog::i("W:H - %d:%d", w, h);
                XClearArea(get_dis(), main_win, 0, 0, 0, 0, true);
            }   break;                
            case ConfigureRequest:
                LOGIEV("ConfigureRequest");
                break;

            case GravityNotify:
                LOGIEV("GravityNotify");
                break;
            case ResizeRequest:
                LOGIEV("ResizeRequest");
                break;
            case CirculateNotify:
                LOGIEV("CirculateNotify");
                break;
            case CirculateRequest:
                LOGIEV("CirculateRequest");
                break;
            case PropertyNotify:
                LOGIEV("PropertyNotify");
                break;

            case SelectionClear:
                LOGIEV("SelectionClear");
                break;
            case SelectionRequest:
                LOGIEV("SelectionRequest");
                break;
            case SelectionNotify:
                LOGIEV("SelectionNotify");
                break;

            case ColormapNotify:
                LOGIEV("ColormapNotify");
                break;

            case ClientMessage:
                LOGIEV("ClientMessage");
                break;

            case MappingNotify:
                LOGIEV("MappingNotify");
                break;
            case GenericEvent:
                LOGIEV("GenericEvent");
                break;
            }
        }
    }

    XUnmapWindow(get_dis(), main_win);
    XDestroyWindow(get_dis(), main_win);
    close_display();

    return 0;
}