
#include "xwin.h"
#include "ckt_types.h"

#include <cstdio>

Display* _dspl = nullptr;

bool open_display()
{
    _dspl = XOpenDisplay(nullptr);
    return !_dspl;
}
void close_display()
{
    if (_dspl)
        XCloseDisplay(_dspl);
}
Display* get_dis()
{
    return _dspl;
}

Window create_window(i32 x, i32 y, i32 w, i32 h, i32 bs, Window parent, ccstr title/*= nullptr*/)
{
    if (!_dspl)
        return 0;

    auto defScr = DefaultScreen(_dspl);
    auto rootWin = DefaultRootWindow(_dspl);
    printf("defScr: %d; rootWin: 0x%08lX\n", defScr, rootWin);

    i32 scrBitDepth = 24;
    XVisualInfo visInfo = {};
    if (!XMatchVisualInfo(_dspl, defScr, scrBitDepth, TrueColor, &visInfo))
    {
        return 0;
    }
    
    XSetWindowAttributes winAttr = {};

    if (parent == 0)
    {   // main app window
        winAttr.event_mask = 
                StructureNotifyMask |
                SubstructureNotifyMask |
                SubstructureRedirectMask |
                // ResizeRedirectMask |
                ExposureMask |
                ButtonPressMask |
                ButtonReleaseMask |
                KeyPressMask |
                KeyReleaseMask |
                EnterWindowMask |
                LeaveWindowMask |
                VisibilityChangeMask |
                FocusChangeMask;

        winAttr.bit_gravity = StaticGravity;
        winAttr.background_pixel = 0xFF181818;
        winAttr.colormap = XCreateColormap(_dspl, rootWin, visInfo.visual, AllocNone);
        u32 attrMask = CWBackPixel | CWColormap | CWEventMask | CWBitGravity;

        auto win = XCreateWindow(
            _dspl,
            rootWin,    // parent window
            x, y,
            w, h,
            bs, visInfo.depth,
            InputOutput, visInfo.visual,
            attrMask, &winAttr);

        if (title)
            XStoreName(_dspl, win, title);

        XMapWindow(_dspl, win);
        XFlush(_dspl);

        return win;
    }
    else
    {
        XSetWindowAttributes winAttr = {};
        winAttr.event_mask = 
                //StructureNotifyMask |
                //SubstructureNotifyMask |
                //SubstructureRedirectMask |
                //ResizeRedirectMask |
                //ExposureMask |
                ButtonPressMask |
                ButtonReleaseMask |
                //KeyPressMask |
                //KeyReleaseMask |
                EnterWindowMask |
                LeaveWindowMask;
                //VisibilityChangeMask |
                //FocusChangeMask;

        winAttr.background_pixel = 0xFF808080;
        u32 attrMask = CWBackPixel | CWEventMask;

        auto win = XCreateWindow(
            _dspl,
            parent,    // parent window
            x, y,
            w, h,
            bs, visInfo.depth,
            InputOutput, visInfo.visual,
            attrMask, &winAttr);

        if (title)
            XStoreName(_dspl, win, title);

        XMapWindow(_dspl, win);
        XFlush(_dspl);

        return win;
    }

    return 0;
}

void set_size_hint(Window win, i32 minw, i32 minh, i32 maxw, i32 maxh)
{
    XSizeHints hints = {};
    if (minw > 0 && minh > 0) hints.flags |= PMinSize;
    if (maxw > 0 && maxh > 0) hints.flags |= PMaxSize;

    hints.min_width = minw;
    hints.min_height = minh;
    hints.max_width = maxw;
    hints.max_height = maxh;

    XSetWMNormalHints(_dspl, win, &hints);
}