
files := *.cpp
appname := app
logname := log.txt
buildpath := build/

$(buildpath)$(appname): $(files)
	@echo "Release build.."
	@echo $?	#prints all changed prerequsite files
	@mkdir -p $(buildpath)
	@g++ -Wall -lX11 -O3 -o $(buildpath)$(appname) $(files)
	@echo "done"

d: $(files)
	@echo "Release build.."
	@echo $?	#prints all changed prerequsite files
	@g++ -Wall -lX11 -g -Og d -o $(buildpath)d $(files)

clean:
	@echo "cleaning up.."
	@rm -f -r $(buildpath)
	@rm -f -r $(logname)
	@rm -f -r d
	@rm -f -r *.o
	@echo "done"
