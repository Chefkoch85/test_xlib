
#include "log.h"
#include "ckt_types.h"

#include <bits/types/struct_timespec.h>
#include <bits/types/time_t.h>
#include <cstdio>
#include <cstdarg>
#include <ctime>

FILE* CLog::_file = nullptr;


CLog::CLog()
{

}
CLog::~CLog()
{
    if (_file)
        fclose(_file);
}

bool CLog::open(ccstr filename)
{
    _file = std::fopen(filename, "w");
    return false;
}

void CLog::i(ccstr fmt, ...)
{
    if (!_file)
        return;

    va_list args;
    va_start(args, fmt);

    log_time();
    fprintf(_file, "info: ");
    vfprintf(_file, fmt, args);
    fprintf(_file, "\n");

    va_end(args);
}
void CLog::w(ccstr fmt, ...)
{
    if (!_file)
        return;

    va_list args;
    va_start(args, fmt);

    log_time();
    fprintf(_file, "warn: ");
    vfprintf(_file, fmt, args);
    fprintf(_file, "\n");

    va_end(args);
}
void CLog::e(ccstr fmt, ...)
{
    if (!_file)
        return;

    va_list args;
    va_start(args, fmt);

    log_time();
    fprintf(_file, "err : ");
    vfprintf(_file, fmt, args);
    fprintf(_file, "\n");

    va_end(args);
}
void CLog::c(bool should_exit, ccstr fmt, ...)
{
    if (!_file)
        return;

    va_list args;
    va_start(args, fmt);

    log_time();
    fprintf(_file, "crit: ");
    vfprintf(_file, fmt, args);
    fprintf(_file, "\n");

    va_end(args);

    if (should_exit)
        exit(1);
}


/// +++ private +++ ///

void CLog::log_time()
{
    if (!_file)
        return;

    timespec ts = {};
    clock_gettime(CLOCK_REALTIME, &ts);
    auto tm = localtime(&ts.tv_sec);
    u64 ms = ts.tv_nsec / 1000 / 1000;

    fprintf(_file, "%02d:%02d:%02d.%04ld -> ", tm->tm_hour, tm->tm_min, tm->tm_sec, ms);
}