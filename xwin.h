#pragma once

#include "ckt_types.h"

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>

bool open_display();
void close_display();
Display* get_dis();

Window create_window(i32 x, i32 y, i32 w, i32 h, i32 bs, Window parent, ccstr title = nullptr);
void set_size_hint(Window win, i32 minw, i32 minh, i32 maxw, i32 maxh);


