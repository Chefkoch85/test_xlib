#pragma once

#include "ckt_types.h"
#include <cstdio>

class CLog
{
public:
    CLog();
    ~CLog();

    static bool open(ccstr filename);

    static void i(ccstr fmt, ...);
    static void w(ccstr fmt, ...);
    static void e(ccstr fmt, ...);
    static void c(bool should_exit, ccstr fmt, ...);

private:
    static void log_time();
private:
    static FILE* _file;
};